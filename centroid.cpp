#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;

void beolvas(int n, int m,int data[3823][65]){
    FILE *f;
    if (n > 2000){
        f = fopen("optdigits.tra", "a+");
    } else {
        f = fopen("optdigits.tes", "a+");
    }
    int a;
    for (int i = 0; i < n;i++){
        for (int j = 0; j < m;j++){
            fscanf(f,"%d,",&a);
            data[i][j] = a;
        }
    }
}

float euklides(double a[], int b[]){
    int distance = 0;
    for (int i = 0;i < 64;i++){
        int tmp = b[i]-a[i];
        tmp *= tmp;
        distance += tmp;
    }
    return sqrt(distance);
}

void avg(int t[][65], int n,double to[10][64]){
    int all[10] = {0};
    for (int i= 0;i<n;i++){
        all[t[i][64]]++;
        for(int j = 0;j < 64;j++){
            to[t[i][64]][j] += t[i][j];
        }
    }
    for (int i = 0; i < 10; i++){
        for(int j = 0; j < 64; j++){
            to[i][j] /= all[i];
        }
    }
}

void calculate(int t[][65], int n, double avg[10][64]){
    int all[10] = {0};
    int current[10] = {0};
    for (int i = 0; i < n; i++){
        all[t[i][64]]++;
        int dist = 99999;
        int num = 0;
        for (int j = 0; j < 10; j++){
            if (euklides(avg[j],t[i]) < dist){
                dist = euklides(avg[j],t[i]);
                num = j;
            }
        }
        if (num == t[i][64]){
            current[t[i][64]]++;
        }
    }

    double cu = 0;
    double al = 0;

    for (int i = 0; i < 10; i++){
        cu += current[i];
        al += all[i];
    }

    cout << 100-(cu/al*100) << endl;
}

int main(){
    double avarage[10][64];
    int trainingData[3823][65] = {0};
    int testData[1797][65] = {0};
    beolvas(3823,65,trainingData);
    beolvas(1797,65,testData);
    avg(trainingData,3823,avarage);
    cout << "Test hiba:\t";
    calculate(testData,1797,avarage);
    cout << "Tanulasi hiba:\t";
    calculate(trainingData,3823,avarage);
    return 0;
}